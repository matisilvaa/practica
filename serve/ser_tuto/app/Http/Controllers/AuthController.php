<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use JWTAuth;
use App\User;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{
    public function userAuth(Request $request){
    	$credentials = $request->only('email','password');
    	$token = null;
    	try{
    		if (!$token = JWTAuth::attempt($credentials)){
    			return response()->json(['error' => 'invalid_crendentials']);
    		}
    	}catch (JWTException $e){
    		return response()->json(['error' => 'somthing_went_wrong'], 500);
    	}
    	$users = User::where('email', '=', $request->email, 'AND', 'password', '=', $request->password)->first();
    	
    	return response()->json(['token' => $token, 'id_user' => $users->id]);
    }
}
