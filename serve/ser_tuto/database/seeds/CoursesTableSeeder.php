<?php

use Illuminate\Database\Seeder;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses') ->insert([
        	'author' => 'Matias',
        	'name' => 'Antonio',
        	'description' => 'Course very good',
        	'price' => 5000,
        	]);
    }
}
